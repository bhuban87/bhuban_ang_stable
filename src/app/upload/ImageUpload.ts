import { WINDOW } from '@ng-toolkit/universal';
import { Component , Inject} from '@angular/core';
import { Http, RequestOptions, Headers, Response } from '@angular/http';  
import { Observable, Subject } from 'rxjs';
import { map, take } from 'rxjs/operators';
import {StoreService} from '../services/store.service';
import { ItemsBean } from '../beans/MyItem';
import { ItemOrderBean } from '../beans/ItemOrderBean';
import { Constants } from '../common/common.component';
@Component ({
   selector: 'image-upload',
   templateUrl: './ImageUpload.html',
   providers:[StoreService]
 
})
export   class   ImageUpload  {
   
    private isUploadBtn: boolean = true;  
    url=null;
    itemDesc=null;
    itemName=null;
    itemColor=null;
    availNumber=null;
    itemPrice=null;
    files: Array<File> = [];
    formData: FormData = new FormData();  
    shopItems=null;
    catagorySelection=null;
    subcatagoryList=null;
    subcatagorySelected=null;
    offset=20;
    intialPage=0;
    adminUpdateSpinner=null;
    updateItemMessage=null;
    searchCriteriaObj={
        "document":null,
        "subDoc":null,
        "page":this.intialPage,
        "offset":this.offset,
        "colors":null,
        "minprice":null,
        "maxprice":null,
        "pricesort":null,
        "itemId":null

     }
     itemId=null;
     showSpinner=false;
     totalElemnts:number=null;
     result=null;
     itetArray:ItemsBean[]=[];
     itemList:any=null;
     selectedItem=null;
     updateRequest={
         "chnagePrice":null,
         "changeAvailableNumber":null,
         "changeDiscount":null,
         "catagory":null,
         "subCatagory":null,
         "itemId":null
     }
     itemOrderList=[];
     itemOrderResult=null;
    constructor(@Inject(WINDOW) private window: Window, private http: Http,private store:StoreService) {  

        this.shopItems={
            "catagories":[
                            {
                              "catagory":"MensCloth",
                              "subcatagory":
                              [
                                {
                                  "key":"MENS_SHIRTS",
                                  "value":"Shirts"
                                },
                                {
                                  "key":"MENS_TSHIRTS",
                                  "value":"T-Shirts"
                                },
                                {
                                  "key":"MensKurta",
                                  "value":"Kurta"
                                },
                                {
                                  "key":"MensJacket",
                                  "value":"Jacket"
                                }
                                
                              ]
                              
                            },
                            {
                              "catagory":"Bedding",
                              "subcatagory":[
                                              {"key":"Pillow",
                                                "value":"Pillow"
                                              },
                                              {"key":"BedSheets",
                                                "value":"BedSheets"
                                            }
                                          ]
                              
                            },
                            {
                              "catagory":"Watches",
                              "subcatagory":[
                                {
                                  "key":"FastTrack",
                                  "value":"FastTrack"
                                },
                                {
                                  "key":"Casio",
                                  "value":"Casio"
                                },
                                {
                                  "key":"Fossil",
                                  "value":"Fossil"
                                }
                               
                              ]
                              
                            },
                            {
                              "catagory":"WomensCloth",
                              "subcatagory":[
                                {
                                  "key":"Sharee",
                                  "value":"Sharee"
                                },
                                {
                                  "key":"womensKurta",
                                  "value":"Kurta"
                                },
                                {
                                  "key":"womensJeans",
                                  "value":"Jeans"
                                }
                              
                              ]
                              
                            }
                        
                          ]
          };
    }  
    //file upload event  
    fileChange(event)
    {  
            
            let fileList: FileList = event.target.files;  
            if (fileList.length == 1) 
            {  
                let file: File = fileList[0]; 
                this.files.push(file) ;
                var reader = new FileReader();

                reader.readAsDataURL(event.target.files[0]); // read file as data url
                
                reader.onload = (event) => { // called once readAsDataURL is completed
                   // this.url = event.target.value;
                  this.url = event.target;
                }
                this.formData.append('files', file, file.name);
            }
            else if(fileList.length >1)
            {
                for(let i=0;i<fileList.length;i++)
                {
                    let file: File = fileList[i]; 
                    this.files.push(file) ;
                   this. formData.append('files', file, file.name);
                }
               
            }

           
           

            
            
        // window.location.reload();  
    }  
    uploadItemDetails()
    {
        let headers = new Headers()  
      this. formData.append('itemName', this.itemName);
        this. formData.append('itemColor', this.itemColor);
        this. formData.append('availbleNumber', this.availNumber);
        this. formData.append('itemDescription', this.itemDesc);
        this. formData.append('itemPrice', this.itemPrice);
        this. formData.append('itemCatagory', this.catagorySelection);
        this. formData.append('itemSubCatagory', this.subcatagorySelected);
        
        
        
            //headers.append('Content-Type', 'json');  
            //headers.append('Accept', 'application/json');  
            let options = new RequestOptions({ headers: headers });  
            let apiUrl1 = Constants.API_ENDPOINT+"uploadMultipleFiles";  
           // this.http.post(apiUrl1, this.formData, options)  
           this.http.post(apiUrl1, this.formData, options)  
            .subscribe
            (  
                    data => {
                        console.log('success');
                        this.window.location.reload(); 
                    },  
                    error => {
                        console.log(error)
                    }  
                    
            )  
            
    }
    onSelect(event)
    {
        this.catagorySelection=event;
        console.log("hello select event"+this.catagorySelection);
        this.shopItems.catagories.forEach((item, index) => {
            if (item.catagory == this.catagorySelection) {
                this.subcatagoryList=item.subcatagory;
                
             }
           
          });

    }
    onSelect1(event)
    {
        this.subcatagorySelected=event;
        console.log("hello select event1"+this.subcatagorySelected);
    }
      //getItemInformation(page,offest,document,subDoc)
      getItemInformation(searchCriteriaObj)
      {
          this.showSpinner=true;
          this.store.getBackendCall(searchCriteriaObj)
              .subscribe(
                    data => {
                          //assigning service response 
                          this.totalElemnts=data.totalElements;
                          this.showSpinner=false;
                        this.result = data.content;
                        if(this.result instanceof Array)
                        {
                            this.result.forEach(element => {
                                var catagory=element.itemCatagory;
                                var subcatagory=element.subcatagory;
                                var itemObj=new ItemsBean(element.itemId,element.itemDescription,
                                    element.itemPrice,element.pics,element.availbleNumber,catagory,subcatagory,element.discountPercentage);
                                  
                                    this.itetArray.push(itemObj);
                            });
                            this.itemList=this.itetArray;
                            this.itetArray=[];
                        }
                       
                    },  
                    error => {
                      this.showSpinner=false;
                        console.log(error)
                    }  
             
              )
      }
      page:any;
      getProductsInfoData(event)
      {
        this.page=event;
      
        this.searchCriteriaObj.page=event-1;
       
       // this. getItemInformation(event-1,7,'MensCloth','getMensShirts');
       this. getItemInformation(this.searchCriteriaObj);
      }
    search()
    {

        this.searchCriteriaObj.document=this.catagorySelection;
        this.searchCriteriaObj.subDoc=this.subcatagorySelected;
        this. searchCriteriaObj.itemId=this.itemId;
   
        this.updateRequest.catagory=this.catagorySelection;
        this.updateRequest.subCatagory=this.subcatagorySelected;
       this.getItemInformation(this.searchCriteriaObj);
    }
    selectItem(item)
    {
     
        this.selectedItem=item;
        this.updateRequest.itemId= this.selectedItem.itemId;
        this.updateRequest.chnagePrice=this.selectedItem.itemPrice;
        this.updateRequest.changeAvailableNumber=this.selectedItem.itemQuantity;
        this.updateRequest.changeDiscount= this.selectedItem.discountValue;
    }
    priceChange(data)
    {
       
        this.updateRequest.chnagePrice=data.srcElement.valueAsNumber;
    }
    quantityChange(data)
    {
        this.updateRequest.changeAvailableNumber=data.srcElement.valueAsNumber;
    }
    discountChange(data)
    {
        this.updateRequest.changeDiscount=data.srcElement.valueAsNumber;
    }
    updateItem()
    {
        console.log("this.updateRequest"+this.updateRequest);
        this.adminUpdateSpinner=true;
        this.updateItemMessage=null;
        this.store.updateStoreItemsInfo(this.updateRequest).subscribe(res => { 
           this.updateItemMessage="Item saved suucessfully";
            console.log("Item saved suucessfully");		
            this.adminUpdateSpinner=false;
              this.updateRequest={
                "chnagePrice":null,
                "changeAvailableNumber":null,
                "changeDiscount":null,
                "catagory":null,
                "subCatagory":null,
                "itemId":null
            }
          },
          err => {
            console.log(err);
            this.adminUpdateSpinner=false;
            this.updateRequest={
              "chnagePrice":null,
              "changeAvailableNumber":null,
              "changeDiscount":null,
              "catagory":null,
              "subCatagory":null,
              "itemId":null
          }
            this.updateItemMessage="Some error in updating";
          }
       );
    }

    getLatestOrders()
    {
        
        var req={
          "initial":0,
          "offset":10
      };
      this.store.getLatestOrders(req)  .subscribe(
          out => {
                //assigning service response 
                this.itemOrderList =[];
                
              
                this.itemOrderResult = out.content;
                if(this.itemOrderResult instanceof Array)
                {
                    this.itemOrderResult.forEach(data => {
                        var itemOrder= new ItemOrderBean(data.orderId, data.userId,data.quantity,data.document,data.subDocument,data.custInfo) ;
                            this.itemOrderList.push(itemOrder);
                    });
                  }
                 this.itemOrderResult=null;
               
              
          },  
          error => {
            //this.showSpinner=false;
              console.log(error)
          } 
        );
    }
}