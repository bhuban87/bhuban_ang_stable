import { WINDOW } from '@ng-toolkit/universal';
import { Component, NgZone , Inject} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NgxGalleryAnimation, NgxGalleryImage, NgxGalleryOptions } from 'ngx-gallery';
import { ItemsBean } from './beans/MyItem';
import { CartDataService } from './cart/cartDataService';
import { PaggingDataItems } from './services/pagging.dataItems';
import { StoreService } from './services/store.service';


@Component ({
   selector: 'app-root',
   templateUrl: './selectedProduct.html'  
})

export  class SelectedProductItem  {
    selectedProduct:any;
    element: any;
    selectedImagePick:String;
    cartCount:number;
    selectedItem:ItemsBean=null;
    testhtml=null;
    //@Output() valueChange = new EventEmitter();
    items: Array<any> = [];
    showSpinner=true;
    public imageSources: string[] = [
        "assets/images/a1.jpg","assets/images/amazon_2.jpg","assets/images/amazon_1.jpg"
     ];
    /* 
     public config: ICarouselConfig = {
       verifyBeforeLoad: true,
       log: false,
       animation: true,
       animationType: AnimationConfig.SLIDE,
       autoplay: true,
       autoplayDelay: 2000,
       stopAutoplayMinWidth: 768
     };*/
  
     galleryOptions: NgxGalleryOptions[];
     galleryImages: NgxGalleryImage[];
    constructor
    (private zone: NgZone,
      private cartSevice:CartDataService,private paramRoute: ActivatedRoute,
      private store:StoreService,private paggingService:PaggingDataItems
    )
     { 
        let selectedInfo={
            "pics":["assets/images/a1.jpg","assets/images/amazon_2.jpg","assets/images/amazon_1.jpg"]
            
        };
        this.selectedProduct=selectedInfo;
        this.items = [
          { name: 'assets/images/a1.jpg' },
          { name: 'assets/images/amazon_2.jpg' },
          { name: 'assets/images/amazon_1.jpg' },
          { name: 'assets/images/1.jpeg' },
          { name: 'assets/images/2.jpeg' }
        
        ];

               //let itemId = this.paramRoute.snapshot.queryParams["itemId"];
               let itemId = this.paramRoute.snapshot.params["itemId"];
               let catagory = this.paramRoute.snapshot.params["catagory"];
               let subcatagory = this.paramRoute.snapshot.params["subcatagory"];
               // this.selectedItem=this.store.getSelectedItem(itemId);
               this.selectedItem=this.paggingService.getSelectedItemFromCache(Number(itemId));
               this.galleryOptions = [
                {
                    width: '44%',
                    height: '533px',
                    thumbnailsColumns: 4,
                    imageAnimation: NgxGalleryAnimation.Zoom,
                    previewZoom:true,
                    previewRotate:true,
                    imagePercent: 100,
                    previewKeyboardNavigation:true,
                    thumbnailMargin: 2,
                    thumbnailsMargin: 2            
                },
                // max-width 800
                {
                    breakpoint: 1200,
                    width: '80%',
                    //height: '600px',
                    preview: false,
                    imagePercent: 100,
                    thumbnailsPercent: 20,
                    thumbnailsMargin: 20,
                    thumbnailMargin: 20,
                    previewZoom:true,
                    previewRotate:true,
                    previewKeyboardNavigation:true
                },
                // max-width 400
                {
                    breakpoint: 400,
                    // width: '34%',
                    // height: '233px',
                    preview: false
                }
                
            ];
               if(this.selectedItem==null ||this.selectedItem===undefined)
               {
                   var searchCriteriaObj={
                       "document":catagory,
                       "subDoc":subcatagory,
                       "page":0,
                       "offset":2,
                       "itemId":Number(itemId)
           
                   };
               this.getItemInformation(searchCriteriaObj);
               }
               else
               {
                this.renderImageData();
                this.showSpinner=false;
               }
    
              // this.selectedImagePick=this.selectedItem.imageLocation[0];
               console.log("itemid passed ::"+itemId);
     }

    
   
   ngOnInit() 
     {
        // this.paramRoute.snapshot.p
        
 

        
        

            /*this.galleryImages = [
                {
                    small: 'assets/images/2.jpeg',
                    medium: 'assets/images/2.jpeg',
                    big: 'assets/images/2.jpeg',
                    label:'hello bhuban'
                },
                {
                    small: 'assets/images/5.jpeg',
                    medium: 'assets/images/5.jpeg',
                    big: 'assets/images/5.jpeg'
                },
                {
                    small: 'assets/images/1.jpeg',
                    medium: 'assets/images/1.jpeg',
                    big: 'assets/images/1.jpeg'
                },
                {
                    small: 'assets/images/3.jpeg',
                    medium: 'assets/images/3.jpeg',
                    big: 'assets/images/3.jpeg'
                },
                {
                    small: 'assets/images/4.jpeg',
                    medium: 'assets/images/4.jpeg',
                    big: 'assets/images/4.jpeg'
                }
            ];*/

     }
     result=[];
     getItemInformation(searchCriteriaObj)
     {
        
         this.store.getBackendCall(searchCriteriaObj)
             .subscribe(
                   data => {
                         //assigning service response 
                       
                       this.result = data.content;
                       if(this.result instanceof Array)
                       {
                           this.result.forEach(element => {
                               var catagory=element.itemCatagory;
                               var subcatagory=element.subcatagory;
                              this.selectedItem=new ItemsBean(element.itemId,element.itemDescription,
                                   element.itemPrice,element.pics,element.availbleNumber,catagory,subcatagory,element.discountPercentage);
                                  
                           });
                         
                       }
                    this.renderImageData();
                    this.showSpinner=false;
                   },  
                   error => {
                    
                       console.log(error);
                       this.showSpinner=false;
                   }  
            
             )
     }

     renderImageData()
     {
        this.galleryImages = [];
        let imageArraySize=this.selectedItem.imageLocation.length;
        for(var i=0;i<imageArraySize;i++)
        {
            let imageObject={
                small: this.selectedItem.imageLocation[i],
                medium: this.selectedItem.imageLocation[i],
                big: this.selectedItem.imageLocation[i],
                label:'hello bhuban'
            };
            this.galleryImages.push(imageObject);
        }
        console.log("galleryImages"+this.galleryImages);
        this.galleryOptions = [
            {
                width: '44%',
                height: '533px',
                thumbnailsColumns: 4,
                imageAnimation: NgxGalleryAnimation.Zoom,
                previewZoom:true,
                previewRotate:true,
                imagePercent: 100,
                previewKeyboardNavigation:true,
                thumbnailMargin: 2,
                thumbnailsMargin: 2            
            },
            // max-width 800
            {
                breakpoint: 1200,
                width: '80%',
                //height: '600px',
                preview: false,
                imagePercent: 100,
                thumbnailsPercent: 20,
                thumbnailsMargin: 20,
                thumbnailMargin: 20,
                previewZoom:true,
                previewRotate:true,
                previewKeyboardNavigation:true
            },
            // max-width 400
            {
                breakpoint: 400,
                // width: '34%',
                // height: '233px',
                preview: false
            }
            
        ];
     }
     
     mouseDown(event) {
   
          this.element = event.target;
          this.selectedImagePick= this.element.src;
         /*  this.zone.runOutsideAngular(() => {
            window.document.addEventListener('mousemove', this.mouseMove.bind(this));
          }); */
        }
      
        mouseMove(event) {
          event.preventDefault(); 
         var largeimg=this.selectedImagePick;
          this.testhtml = "dddddd";
        
          this.element = event.target;
         // this.element.setAttribute('height', event.clientX + 'px');
         //this.element.setAttribute('width', event.clientX  + 'px');
        }
       
        onMouseOut(): void {
          //this.testhtml = "<p>Hello worleeeeeeeeeeeeeeeeeeed</p>";
        }
        addCart()
        {
          this.cartSevice.increment();
          this.cartSevice.addItems(this.selectedItem,null);
          this.cartCount=this.cartSevice.count;
         // this.cartCount=this.cartSevice.count;
          //this.valueChange.emit(this.cartCount);
        }

}