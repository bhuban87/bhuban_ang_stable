import { Component ,ViewChild } from '@angular/core';
import { AppComponent } from './app.component';
import { NgxImageGalleryComponent, GALLERY_IMAGE, GALLERY_CONF } from "ngx-image-gallery";
import { NgxGalleryModule } from 'ngx-gallery';
import { NgxGalleryOptions, NgxGalleryImage, NgxGalleryAnimation } from 'ngx-gallery';

import {UserInfo} from './userModule/user.info';

@Component ({
   selector: 'app-root',
   templateUrl: './product1.html',
   providers:[AppComponent]
})
export   class   Product1  {
    hello=null;
    numItemSelected=1;
    intialItemValue=15;
    updatedValue=null;
    itemValue=null;
    @ViewChild(NgxImageGalleryComponent) ngxImageGallery: NgxImageGalleryComponent;
    constructor(private parentData: AppComponent) {
       // this.hello=this.parentData.second;
        this.updatedValue=this.intialItemValue*this.numItemSelected;
    }
    valueChange(event)
    {
        let x=10;
        this.numItemSelected=event.target.value;
        this.updatedValue=this.intialItemValue*this.numItemSelected;
    }


   
  
    // gallery configuration
    conf: GALLERY_CONF = {
      imageOffset: '0px',
      showDeleteControl: false,
      showImageTitle: false,
    };
      
    // gallery images
    images: GALLERY_IMAGE[] = [
      {
        url: "https://images.pexels.com/photos/669013/pexels-photo-669013.jpeg?w=1260", 
        altText: 'woman-in-black-blazer-holding-blue-cup', 
        title: 'woman-in-black-blazer-holding-blue-cup',
        thumbnailUrl: "https://images.pexels.com/photos/669013/pexels-photo-669013.jpeg?w=60"
      },
      {
        url: "https://images.pexels.com/photos/669006/pexels-photo-669006.jpeg?w=1260", 
        altText: 'two-woman-standing-on-the-ground-and-staring-at-the-mountain', 
        extUrl: 'https://www.pexels.com/photo/two-woman-standing-on-the-ground-and-staring-at-the-mountain-669006/',
        thumbnailUrl: "https://images.pexels.com/photos/669006/pexels-photo-669006.jpeg?w=60"
      },
    ];
   
    galleryOptions: NgxGalleryOptions[];
    galleryImages: NgxGalleryImage[];
   
    ngOnInit() {
        this.galleryOptions = [
           
            
            {
                width: '440px',
                height: '610px',
                thumbnailsColumns: 4,
                imageAnimation: NgxGalleryAnimation.Zoom,
                previewZoom:true,
                previewRotate:true,
                imagePercent: 100,
                previewKeyboardNavigation:true,
                thumbnailMargin: 2, thumbnailsMargin: 2 
            },
            // max-width 800
            {
                breakpoint: 800,
                width: '100%',
                height: '600px',
                imagePercent: 80,
                thumbnailsPercent: 20,
                thumbnailsMargin: 20,
                thumbnailMargin: 20,
                previewZoom:true,
                previewRotate:true,
                previewKeyboardNavigation:true
            },
            // max-width 400
            {
                breakpoint: 400,
                preview: false
            }
            
        ];
 
        this.galleryImages = [
            {
                small: 'assets/images/2.jpeg',
                medium: 'assets/images/2.jpeg',
                big: 'assets/images/2.jpeg',
                label:'hello bhuban'
            },
            {
                small: 'assets/images/5.jpeg',
                medium: 'assets/images/5.jpeg',
                big: 'assets/images/5.jpeg'
            },
            {
                small: 'assets/images/1.jpeg',
                medium: 'assets/images/1.jpeg',
                big: 'assets/images/1.jpeg'
            },
            {
                small: 'assets/images/3.jpeg',
                medium: 'assets/images/3.jpeg',
                big: 'assets/images/3.jpeg'
            },
            {
                small: 'assets/images/4.jpeg',
                medium: 'assets/images/4.jpeg',
                big: 'assets/images/4.jpeg'
            }
        ];
        

    }
      
    // METHODS
    // open gallery
    openGallery(index: number = 0) {
      this.ngxImageGallery.open(index);
    }
      
    // close gallery
    closeGallery() {
      this.ngxImageGallery.close();
    }
      
    // set new active(visible) image in gallery
    newImage(index: number = 0) {
      this.ngxImageGallery.setActiveImage(index);
    }
      
    // next image in gallery
    nextImage(index) {
      this.ngxImageGallery.next();
    }
      
    // prev image in gallery
    prevImage(index) {
      this.ngxImageGallery.prev();
    }
      
    /**************************************************/
      
    // EVENTS
    // callback on gallery opened
    galleryOpened(index) {
      console.info('Gallery opened at index ', index);
    }
   
    // callback on gallery closed
    galleryClosed() {
      console.info('Gallery closed.');
    }
   
    // callback on gallery image clicked
    galleryImageClicked(index) {
      console.info('Gallery image clicked with index ', index);
    }
    
    // callback on gallery image changed
    galleryImageChanged(index) {
      console.info('Gallery image changed to index ', index);
    }
   
    // callback on user clicked delete button
    deleteImage(index) {
      console.info('Delete image at index ', index);
    }
}