import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-common',
  templateUrl: './common.component.html',
  styleUrls: ['./common.component.css']
})
export class CommonComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
export class Constants{
 // public static API_ENDPOINT='http://ec2-3-16-216-19.us-east-2.compute.amazonaws.com:8080/';
  //public static API_ENDPOINT="bhubanecomm-511723409.us-east-2.elb.amazonaws.com";
 public static API_ENDPOINT='http://localhost:8080/';
}