import { Injectable } from '@angular/core';
import { Http , Response } from '@angular/http';
import { Observable } from 'rxjs';

//import 'rxjs/add/operator/do';

//import 'rxjs/add/operator/catch'; 
import { map } from 'rxjs/operators';
import { IProduct } from './product';

@Injectable()
export class ProductService {
  // private _producturl='assets/product.json';
  private _producturl="http://localhost:8081/getProduct";
   constructor(private _http: Http){}
   
   getproducts(): Observable<IProduct[]> {
      /*return this._http.get(this._producturl)
      .map((response: Response) => <IProduct[]> response.json())
      .do(data => console.log(JSON.stringify(data)))
      .catch(this.handleError); */
      return null;
   }
   private data=[{
    "ProductID": 1,
    "ProductName": "ProductA"
 },
 
 {
    "ProductID": 2,
    "ProductName": "ProductB"
 }];
   private handleError(error: Response) { 
    console.error(error); 
    return Observable.throw(error.json().error()); 
 } 
}