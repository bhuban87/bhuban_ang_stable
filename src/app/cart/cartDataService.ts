import {Injectable} from '@angular/core';
import{CartType} from './CartType';
import { Observable, Subject } from 'rxjs';
import { ItemsBean } from '../beans/MyItem';
@Injectable()
export class CartDataService {
      private credentials:CartType[] = [];

      getCredentials():CartType[] {
        return this.credentials;
      }
    
      setCredentials(credentials:CartType) {
        this.credentials.push(credentials);
      }
      /*public credentials:string='';
      constructor() { }
      getCredentials():string {
        return this.credentials;
      }
    
      setCredentials(credentialSet:string) {
        this.credentials=credentialSet;
      }*/
      constructor() { }
      private CountNumber = new Subject<Number>();  
      CountNumber$ = this.CountNumber.asObservable();
    public count = 0;
  
    get() {
      return this.count;
    }
  
    increment() {
      this.count++;  
      this.CountNumber.next(this.count);
    }
    
    decrement() {
      this.count--;
    }
    private selectedItemsMap=new Map();
    private selectedItems = new Subject<Number>();  
    selectedItems$ = this.selectedItems.asObservable();
    public itemCount = 0;
  
    getCartItemsList() {
      return this.selectedItemsMap;
    }
    getCartItemsCount() {
      let count=0;
      this.selectedItemsMap.forEach(element => {
        count=count+element.itemQuantity
      });
      this.itemCount=count;
      //return count;
    }

    addItems(ItemsBean,numberOfItems) 
    {
      
      if(this.selectedItemsMap.has(ItemsBean.itemId))
      {
        let updatedItemBean:ItemsBean=this.selectedItemsMap.get(ItemsBean.itemId);
        if(numberOfItems==null)
          updatedItemBean.itemQuantity=(updatedItemBean.itemQuantity)+1;
        /*if(ItemsBean.itemQuantity>=updatedItemBean.itemQuantity)
        {
          updatedItemBean.itemQuantity=(updatedItemBean.itemQuantity)+1;
          this.itemCount++; 
        }
        else
        {
          updatedItemBean.itemQuantity=(updatedItemBean.itemQuantity)-1;
          this.itemCount--; 
        }*/
        this.selectedItemsMap.set(ItemsBean.itemId,updatedItemBean);
      }
      else
      {
        
         ItemsBean.itemQuantity=1;
        this.selectedItemsMap.set(ItemsBean.itemId,ItemsBean);
      }
     
      this.getCartItemsCount();
      this.selectedItems.next(this.itemCount);
    }
    
    delteItems(itemId) 
    {
      this.selectedItemsMap.delete(itemId);
     // this.itemCount--;
     this.getCartItemsCount();
      this.selectedItems.next(this.itemCount);
    }
  
  
}