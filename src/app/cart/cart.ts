import { Component, EventEmitter, Input, Output,ViewChild, ElementRef, AfterViewInit  } from '@angular/core';
/*import { Product3 } from '../product3';
import {ActivatedRoute} from "@angular/router";
import {StoreService} from '.app/services/store.service';
*/

import { RouterModule, Routes,Router,NavigationExtras ,Params ,ActivatedRoute} from '@angular/router';
import {CartDataService} from './cartDataService';
@Component ({
   selector: 'app-root',
   templateUrl: './cart.html',
   providers:[]
})
export   class   Cart {
    @ViewChild('myId') myId: ElementRef;
    totalAmount:number=0;
    /*
    itemId:number;
    itemName:String;
    itemPrice:number;
    imageLocation:String;
    itemQuantity:number;*/
    carts=[
            {
                "itemId":"ABC_1",
                "imageLocation":"assets/images/amazon_1.jpg",
                "itemName":"ssss",
                "itemPrice":15,
                "itemQuantity":2
            },
            {
                "itemId":"ABC_2",
                "imageLocation":"assets/images/amazon_2.jpg",
                "itemName":"ssss2 vvv bbbbnbnbb gfggfg ffgfg",
                "itemPrice":105,
                "itemQuantity":1
            },
            {
                "itemId":"ABC_3",
                "imageLocation":"assets/images/amazon_1.jpg",
                "itemName":"ssss2 vvv bbbbnbnbb gfggfg ffgfg cdsssdvsv dvssdvs  dsdfs fdsfs sff",
                "itemPrice":115,
                "itemQuantity":1
            }
        ];
        constructor(private cartService:CartDataService,private router: Router) 
            {
                this.carts=[];
                this.cartService.getCartItemsList().forEach((value,key) => {
                    this.carts.push(value);
                });
                this.cartService.getCartItemsList().forEach((item, index) => {
                    var oneItemPrice=(item.itemPrice)*(item.itemQuantity);
                    this.totalAmount=this.totalAmount+oneItemPrice
                   
                  });
            }
    valueChange(event,item)
    {
        var target = event.target || event.srcElement || event.currentTarget;
        var idAttr = target.attributes.id;
        
        var value = idAttr.nodeValue;
        var length=value.length;
        var clickedNumber=value.substring(8,length);
        var indexNumber : number = Number(clickedNumber);

        var textValue=event.srcElement.valueAsNumber;
        item.itemQuantity=textValue;
        this.cartService.addItems(item,textValue);
        this.carts[indexNumber].itemQuantity=textValue;
        this.totalPrice();
        console.log("text value change");
    }
    trackByFn(index, item) {
        return index; // or item.id
      }
      removeItem(item)
      {
        this.cartService.delteItems(item.itemId) ;
        this.carts=[];
        this.cartService.getCartItemsList().forEach((value,key) => {
            this.carts.push(value);
        });
        this.totalPrice();
        console.log("item to be remoed"+item.itemId);
      }
      totalPrice()
      {
        var totalMoney=0;
        this.cartService.getCartItemsList().forEach((item, index) => {
            var oneItemPrice=(item.itemPrice)*(item.itemQuantity);
            totalMoney=totalMoney+oneItemPrice;
           
          });
          this.totalAmount=totalMoney;
      }
      checkOutSelectedItems()
      {
        this.router.navigateByUrl('/checkout'); 
      }
}