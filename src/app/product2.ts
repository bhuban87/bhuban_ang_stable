import { Component, EventEmitter, Input, Output ,Inject } from '@angular/core';
import { Product3 } from './product3';
import {ActivatedRoute} from "@angular/router";
import {StoreService} from './services/store.service';
import {CartDataService} from './cart/cartDataService';
import { RouterModule, Routes,Router,NavigationExtras ,Params } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { ItemsBean } from './beans/MyItem';
import {PaggingDataItems} from './services/pagging.dataItems';

@Component ({
   selector: 'app-root1',
   templateUrl: './product2.html',
   providers:[Product3,StoreService,HttpClient]
})
export   class   Product2 {
    //@Input() 
    ctMsg : string; 
    public firstname: string;
    public lastname: string;
    public mySize:number=20;
    itemList:any;
    colors=["blue","red","green","orange","white","purpule","pink","brown","black"]
    collection:any=[];
    p:any;
    itetArray:ItemsBean[]=[];
    selectedColors=[];
    result=null;
    totalElemnts:number=200;
    documentSearch=null;
    subDocSearch=null;
    showSpinner=true;
    selectProdString:string='/SelectedProduct/';
    searchCriteriaObj=null;
    offset=9;
    intialPage=0;
    pannel=false;
    constructor(private product3: Product3,private route: ActivatedRoute,
        private store:StoreService, private cartServ:CartDataService,private router: Router
      ,private http:HttpClient,private paggingService:PaggingDataItems,private cartSevice:CartDataService) 
      {
        this.ctMsg=this.product3.getMessage();
        this.documentSearch = this.route.snapshot.params["searchDoc"];
        this.subDocSearch=this.route.snapshot.params["searchSubDoc"];
        this.searchCriteriaObj={
           "document":this.documentSearch,
           "subDoc":this.subDocSearch,
           "page":this.intialPage,
           "offset":this.offset,
           "colors":null,
           "minprice":null,
           "maxprice":null,
           "pricesort":null

        }
       
    }

    //getItemInformation(page,offest,document,subDoc)
    getItemInformation(searchCriteriaObj)
    {
        this.showSpinner=true;
        this.store.getBackendCall(searchCriteriaObj)
            .subscribe(
                  data => {
                        //assigning service response 
                        this.totalElemnts=data.totalElements;
                       this.showSpinner=false;
                      this.result = data.content;
                      if(this.result instanceof Array)
                      {
                          this.result.forEach(element => {
                              var catagory=element.itemCatagory;
                              var subcatagory=element.subcatagory;
                              var itemObj=new ItemsBean(element.itemId,element.itemDescription,
                                  element.itemPrice,element.pics,element.availbleNumber,catagory,subcatagory,element.discountPercentage);
                                  this.paggingService.addToPagingItemCache(itemObj);
                                  this.itetArray.push(itemObj);
                          });
                          this.itemList=this.itetArray;
                          this.itetArray=[];
                      }
                      console.log(this.result);
                  },  
                  error => {
                    this.showSpinner=false;
                      console.log(error)
                  }  
           
            )
    }


     ngOnInit() {
        var offest=9;
        var intialPage=0;
        
      // this. getItemInformation(intialPage,offest,'MensCloth','shirts');

        this. getItemInformation(this.searchCriteriaObj);
        this.route.queryParams.subscribe(params => {
            this.firstname = params["firstname"];
            this.lastname = params["lastname"];
            this.ctMsg=this.firstname !=null?(this.firstname  + " "+this.lastname):this.ctMsg;
            this.ctMsg=this.product3.getMessage();
            //this.ctMsg=this.firstname  + " "+this.lastname;
           // this.itemList=this.store.getStoreItemByType(12);
           // this.cartServ.setCredentials("bhuban_muduli_97");
        });

         let param1 = this.route.snapshot.queryParams["itemId"];
         this.documentSearch = this.route.snapshot.params["searchDoc"];
      }
      func()
      {
        //this.cartServ.increment();
      }
      selectedItemProc(event,item)
      {
        this.router.navigateByUrl('/SelectedProduct/'+item.itemId); 
      }
      addToCart(event,item)
      {
        this.cartSevice.increment();
        this.cartSevice.addItems(item,null);
      }
      
      getPageData(event)
      {
        this.p=event;
        this.paggingService.clearCache();
        this.searchCriteriaObj.page=event-1;
       
       // this. getItemInformation(event-1,7,'MensCloth','getMensShirts');
       this. getItemInformation(this.searchCriteriaObj);
       
      }
      colorSelection(color)
      {
          let index=this.selectedColors.indexOf(color);
        if(index!=-1)
        {
           
            this.selectedColors.splice(index,1);
        }
        else
        {
            this.selectedColors.push(color);
        }
        var offest=9;
        var intialPage=0;
        
        this.searchCriteriaObj.colors=this.selectedColors;
      // this. getItemInformation(intialPage,offest,'MensCloth','shirts');

        this. getItemInformation(this.searchCriteriaObj);
      }
      priceSelection(low,high)
      {
          /*if(this.searchCriteriaObj.minprice==null)
          {
            this.searchCriteriaObj.minprice=low;
          }
          if(this.searchCriteriaObj.maxprice==null)
          {
            this.searchCriteriaObj.maxprice=high;
          }
        if(this.searchCriteriaObj.minprice>low)
        {
            this.searchCriteriaObj.minprice=low;
        }
            
        if( this.searchCriteriaObj.maxprice<high)
        {
            this.searchCriteriaObj.maxprice=high;
        }*/
        this.searchCriteriaObj.minprice=low;
        this.searchCriteriaObj.maxprice=high;
        this. getItemInformation(this.searchCriteriaObj);
        console.log("low price"+low+"High price"+high);
      }
      documentSerach()
      {

      }
      subDocSearchMethod()
      {

      }
      priceSearch(param)
      {

      }
}