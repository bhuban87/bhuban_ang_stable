import { Injectable } from '@angular/core';

@Injectable()
export class CountService {
 
 constructor() { }
 
  public count = 0;
 
  get() {
    return this.count;
  }
 
  increment() {
    this.count++;  
  }
  
  decrement() {
    this.count--;
  }
  
}