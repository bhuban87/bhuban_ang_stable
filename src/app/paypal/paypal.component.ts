import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Constants } from '../common/common.component';
@Component({
  selector: 'app-paypal',
  templateUrl: './paypal.component.html',
  styleUrls: ['./paypal.component.css']
})
export class PaypalComponent implements OnInit {

  paymentId=null; payerId=null;
  constructor(private paramRoute: ActivatedRoute,private http: HttpClient) { 
    
    // = this.paramRoute.snapshot.params["paymentId"];
    //this.payerId= this.paramRoute.snapshot.params["payerId"];
    this.paramRoute.queryParams.subscribe(params => {
      this.paymentId= params['paymentId'];
      this.payerId= params['PayerID'];
     // console.log(userId);
    });
   this.completePayment(this.paymentId, this.payerId);
  }
  completePayment(paymentId, payerId) {
    var itemOrder=JSON.parse(localStorage.getItem("itemOrder"));
    console.log("complete payment itemOrder"+JSON.stringify(itemOrder));
    return this.http.post( Constants.API_ENDPOINT+'paypal/complete/payment?paymentId=' + paymentId + '&payerId=' + payerId , itemOrder)
    .subscribe
    (  
            data => {
                console.log('success');
                //window.location.reload(); 
            },  
            error => {
                console.log(error)
            }  
            
    )  
  }
  ngOnInit() {
  }

}
