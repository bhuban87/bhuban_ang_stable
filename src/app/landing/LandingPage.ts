import { Component } from '@angular/core';
import { Router } from '@angular/router';
import {SlideshowModule} from 'ng-simple-slideshow';
import {StoreService} from '../services/store.service';
import { ItemsBean } from '../beans/MyItem';

@Component({
    selector: 'landing-page',
    templateUrl: './landing.html',
    providers: [StoreService]
})
export class LandingPage {
    images=['assets/images/2.jpeg','assets/images/1.jpeg','assets/images/3.jpeg','assets/images/4.jpeg',
    'assets/images/2.jpeg','assets/images/1.jpeg','assets/images/3.jpeg','assets/images/4.jpeg',
    'assets/images/2.jpeg','assets/images/1.jpeg','assets/images/3.jpeg','assets/images/4.jpeg',
    'assets/images/2.jpeg','assets/images/1.jpeg','assets/images/3.jpeg','assets/images/4.jpeg',
    'assets/images/2.jpeg','assets/images/1.jpeg','assets/images/3.jpeg','assets/images/4.jpeg'
];

images1:string[][]=[ ["assets/images/2.jpeg","assets/images/1.jpeg","assets/images/3.jpeg","assets/images/4.jpeg","assets/images/4.jpeg","assets/images/2.jpeg"],
                    ["assets/images/2.jpeg","assets/images/1.jpeg","assets/images/3.jpeg","assets/images/4.jpeg","assets/images/4.jpeg","assets/images/4.jpeg"],
                    ["assets/images/2.jpeg","assets/images/1.jpeg","assets/images/3.jpeg","assets/images/4.jpeg"]    
                ];
 images2:string[][]=[ ["assets/images/2.jpeg","assets/images/1.jpeg"],["assets/images/3.jpeg","assets/images/4.jpeg"],["assets/images/4.jpeg","assets/images/2.jpeg"],
 ["assets/images/2.jpeg","assets/images/1.jpeg"],["assets/images/3.jpeg","assets/images/4.jpeg"],["assets/images/4.jpeg","assets/images/2.jpeg"],
 ["assets/images/2.jpeg","assets/images/1.jpeg"],["assets/images/3.jpeg","assets/images/4.jpeg"],["assets/images/4.jpeg","assets/images/2.jpeg"]    
            ];
    sliderImages:string[]=["assets/images/2.jpeg","assets/images/1.jpeg","assets/images/3.jpeg","assets/images/4.jpeg"];
   newArrivalItems=null;
   newArrivalObjArray=[];
   convertedDesktopAray=[];
   conMobileArray=[];
   conDiscountMobileArray=[];
   convertedDisCountDesktopAray=[];
  disCountObjArray=[];
    constructor(private router: Router,private store:StoreService) {

        this.getNewArriavlItems();
        this.getDiscountedItems();
       
       
    }
    trackByFn1(index, item) {
        return index; // or item.id
      }
    trackByFn(index, item) {
        return index; // or item.id
      }
    fun(item)
    {
    
        if(item && item.itemId)
        {
            this.router.navigateByUrl('/landingPageSelect/'+item.itemId+"/"+item.itemCatagory+"/"+"Sharee");
            //this.router.navigateByUrl('/Product1');
            
        }
       
    }
    getNewArriavlItems()
    {
        var req={
            "initial":0,
            "offset":10
        };
        this.store.getNewArriavlItems(req)  .subscribe(
            data => {
                  //assigning service response 
                  debugger;
                
                this.newArrivalItems = data.content;
                if(this.newArrivalItems instanceof Array)
                {
                    this.newArrivalItems.forEach(element => {
                        var catagory=element.itemCatagory;
                        var subcatagory=element.subcatagory;
                        var itemObj=new ItemsBean(element.itemId,element.itemDescription,
                            element.itemPrice,element.pics,element.availbleNumber,
                            catagory,subcatagory,element.discountPercentage);
                          
                        this.newArrivalObjArray.push(itemObj);
                    });
                    var deepCopyArray=JSON.parse(JSON.stringify(this.newArrivalObjArray));
                     while(deepCopyArray.length)
                    {
                        this.convertedDesktopAray.push(deepCopyArray.splice(0,6));
                       
                    }
                    var deepCopyArray2=JSON.parse(JSON.stringify(this.newArrivalObjArray));
                    while(deepCopyArray2.length)
                    {
                        this.conMobileArray.push(deepCopyArray2.splice(0,2));
                       
                    }
                   
                }
              
                
            },  
            error => {
              //this.showSpinner=false;
                console.log(error)
            }  
     
      )
    }
    getDiscountedItems()
    {
        var req={
            "initial":0,
            "offset":10
        };
        this.store.getDiscountedItems(req)  .subscribe(
            data => {
                  //assigning service response 
                
                this.newArrivalItems = data.content;
                if(this.newArrivalItems instanceof Array)
                {
                    this.newArrivalItems.forEach(element => {
                        var catagory=element.itemCatagory;
                        var subcatagory=element.subcatagory;
                        var itemObj=new ItemsBean(element.itemId,element.itemDescription,
                            element.itemPrice,element.pics,element.availbleNumber,
                            catagory,subcatagory,element.discountPercentage);
                          
                        this.disCountObjArray.push(itemObj);
                    });
                    var deepCopyArray=JSON.parse(JSON.stringify(this.disCountObjArray));
                     while(deepCopyArray.length)
                    {
                        this.convertedDisCountDesktopAray.push(deepCopyArray.splice(0,6));
                       
                    }
                    var deepCopyArray2=JSON.parse(JSON.stringify(this.disCountObjArray));
                    while(deepCopyArray2.length)
                    {
                        this.conDiscountMobileArray.push(deepCopyArray2.splice(0,2));
                       
                    }
                   
                }
               
                
            },  
            error => {
              //this.showSpinner=false;
                console.log(error)
            }  
     
      )
    }
}