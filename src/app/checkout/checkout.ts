import { Component ,NgZone ,EventEmitter, Output ,HostListener} from '@angular/core';

import { ActivatedRoute } from '@angular/router';
import {CartDataService} from '../cart/cartDataService';
import {StoreService} from '../services/store.service';
import { ItemsBean } from '../beans/MyItem';
import {NoSanitizePipe} from '../filter/NoSanitizePipe'
import { map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import {CheckOutService} from './checkout.service'

@Component ({
   selector: 'checkout',
   templateUrl: './checkout.html' ,
   providers: [CheckOutService], 
})

export  class CheckOutItems  {
    carts=null;
    saveOrderList=[];
    
    constructor(private cartService:CartDataService,private route: ActivatedRoute,
        private http: HttpClient,
    private checkOutService:CheckOutService)
    {
        this.carts=[];
        this.cartService.getCartItemsList().forEach((value,key) => {
            this.carts.push(value);
        });
    }
    ConfirmPlaceOrder()
    {
       var totalPrice=0;
        this.carts.forEach((obj)=>
        {
            let innerobj={
                "itemId":obj.itemId,
                "itemQuantity":obj.itemQuantity,
                "itemPrice":obj.itemPrice,
                "itemCatagory":obj.itemCatagory,
                "itemSubcatagory":obj.itemSubcatagory,
                "itemName":obj.itemName
            };
            this.saveOrderList.push(innerobj);
            totalPrice=totalPrice+(obj.itemPrice *obj.itemQuantity);
        });

        var saveObj={
            "itemOrderList":this.saveOrderList,
            "userId":1,
            "totalPrice":totalPrice
        };
        var x=JSON.stringify(saveObj);
        console.log("xx=="+x);
        localStorage.setItem("itemOrder", JSON.stringify(saveObj));
        this.checkOutService.saveOrder(saveObj).subscribe(res => { 
           
            console.log("hello save item");		

            this.saveOrderList=[];
            //window.location.reload();
            var url=res.redirect_url;
            window.location.href=url;
            //window.open(url, "_blank");
          },
          err => {
            console.log(err);
            this.saveOrderList=[];
          }
       );
    }
    changeAddressMode(mode)
    {

    }
}