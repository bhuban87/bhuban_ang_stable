import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import{CheckOutResponse} from './checkout.response'
import { Observable, Subject, asapScheduler, pipe, of, from, interval, merge, fromEvent } from 'rxjs';
import { Constants } from '../common/common.component';
@Injectable()
export class CheckOutService {
   
    constructor(private http: HttpClient) {
    }
    /*
    saveOrder(obj) : Observable<CheckOutResponse>
    {
        return this.http.post<CheckOutResponse>(Constants.API_ENDPOINT+"addOrder", obj);
    }
    */
   saveOrder(obj) : Observable<any>
   {
       return this.http.post<any>(Constants.API_ENDPOINT+"paypal/make/payment", obj);
   }
}