import { Injectable } from '@angular/core';
import { ItemsBean } from '../beans/MyItem';
import { Constants } from '../common/common.component';
import { map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { Observable, Subject, asapScheduler, pipe, of, from, interval, merge, fromEvent } from 'rxjs';
@Injectable()
export class StoreService {
    x=null;
    constructor(private http: HttpClient) { }
    getStoreItem(): ItemsBean { 
        return new ItemsBean(12,"dddd",20,"",null,null,null,null); 
     } 
     getSelectedItem(itemId):ItemsBean
     {
        var selectedItem:ItemsBean=null;
        let ItemsBeanArray=this.getStoreItemByType(12);
        ItemsBeanArray.forEach(element => {
            if(element.itemId==itemId)
            {
                selectedItem= element;
                ItemsBeanArray.length=0;
            }
            
        });
        return selectedItem;
     }

 
    // getBackendCall(pageNumber,totalSize,documentRefer,subDocumentrefer){
        getBackendCall(searchCriteria)
        {
            
            var url=Constants.API_ENDPOINT+"getItems?intitial="+Number(searchCriteria.page)+"&offset="+Number(searchCriteria.offset); 
            if(searchCriteria && searchCriteria.document)
            {
                var url=Constants.API_ENDPOINT+"itemSelect/"+searchCriteria.document+"/"+searchCriteria.subDoc+"?intitial="+Number(searchCriteria.page)+"&offset="+Number(searchCriteria.offset);
            // var url="http://localhost:8080/itemSelect/getMensShirts?intitial="+Number(pageNumber)+"&offset="+Number(totalSize); 
            }
            if(searchCriteria.colors)
            {
                url=url+"&colors="+searchCriteria.colors;
            }
            if(searchCriteria.itemId)
            {
                url=url+"&itemId="+searchCriteria.itemId;
            }
            
            return  this.http
            .get<any>(url)
            .pipe(map(data => data
            ));
     }
    getStoreItemByType(data): ItemsBean []
    { 
        
       var itetArray:ItemsBean[]=[];
      /* for(var i=0;i<9;i++)
       {
        var obj1=new ItemsBean(i+1,"shirts",200+i,"assets/images/amazon_2.jpg");
        itetArray.push(obj1)
       }*/
  

    

     

       for(var i=0;i<12;i++)
       {
        var obj1=null
           if(i==1 || i==6)
           {
            
          // obj1=new ItemsBean(101+i,"Cracking the Coding Interview",200,"http://localhost:8080/downloadFile/1.JPG",null);
           }
           else
           {
            //obj1=new ItemsBean(101+i,"Cracking the Coding Interview",200,"assets/images/"+(i+1)+".jpeg",null);
           }
        
       //var obj1=new ItemsBean(i+1,"shirts",200+i,"assets/images/amazon_2.jpg");
       itetArray.push(obj1)
       }
      console.log("item array::"+itetArray);
        return itetArray;
     }
     updateStoreItemsInfo(updateRequest):Observable<any>
     {
     
        return this.http.put<any>(Constants.API_ENDPOINT+"itemSelect/updateItem/"+updateRequest.itemId, updateRequest);
    
     }

     getNewArriavlItems(req)
     {
        var url=Constants.API_ENDPOINT+"itemSelect/find/newItems?intitial=0&offset=10";
        return  this.http
         .get<any>(url)
         .pipe(map(data => data
         ));
     }
     getDiscountedItems(req)
     {
        var url=Constants.API_ENDPOINT+"itemSelect/find/discount/items?intitial=0&offset=10";
        return  this.http
         .get<any>(url)
         .pipe(map(data => data
         ));
     }
     getLatestOrders(req)
     {
        var url=Constants.API_ENDPOINT+"itemSelect/find/fresh/order?intitial=0&offset=10";
        return  this.http
         .get<any>(url)
         .pipe(map(data => data
         ));
     }
}