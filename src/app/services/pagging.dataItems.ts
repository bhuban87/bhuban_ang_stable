import { Injectable } from '@angular/core';
import { ItemsBean } from '../beans/MyItem';

import { map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { Observable, Subject, asapScheduler, pipe, of, from, interval, merge, fromEvent } from 'rxjs';
@Injectable()
export class PaggingDataItems {
    private pagginItemMap=new Map();
    constructor() { }

    addToPagingItemCache(item)
    {
        this.pagginItemMap.set(item.itemId,item);
    }
    clearCache()
    {
        this.pagginItemMap.clear();
    }
    getSelectedItemFromCache(itemId)
    {
        return  this.pagginItemMap.get(itemId);
    }
}