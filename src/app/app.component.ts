/*import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'bhuban-app';
}*/


import { Component } from '@angular/core';
import { IProduct } from './product';
import { ProductService } from './productservice';
import { ButtonService } from './button.service';
import {StoreService} from './services/store.service';
import { ItemsBean } from './beans/MyItem';
import { VERSION } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CountService }   from './Test/count.service';
import {FooComponent} from './Test/foo.component';
import {BarComponent} from './Test/bar.component';
import {CartDataService} from './cart/cartDataService';
import { RouterModule, Routes,Router,NavigationExtras ,Params } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import * as $ from 'jquery';

@Component({
  selector: 'app-root',
  templateUrl: './navbar.html',
  styleUrls: ['./app.component.css'],
  providers: [ProductService,ButtonService,StoreService,CountService,CartDataService]
})
export class AppComponent {
  title = 'hello world bhuban';
  first="first";
  second="second";
  third="third";
  loginSpinner=null;
  appList: any[] = [{
    "ID": "1",
    "Name": "One"
  },

  {
    "ID": "2",
    "Name": "Two"
  }
  ];
  documentSearch:null;

  iproducts: IProduct[];
  itemList:any;
  deviceInfo = null;
  shopItems=null;
   constructor(private _product: ProductService,private buttonService:ButtonService,
    private store:StoreService ,private router: Router
    
    ) 
    {
      this.epicFunction();
      this.shopItems={
        "catagories":[
                        {
                          "catagory":"MensCloth",
                          "subcatagory":
                          [
                            {
                              "key":"MENS_SHIRTS",
                              "value":"Shirts"
                            },
                            {
                              "key":"MENS_TSHIRTS",
                              "value":"T-Shirts"
                            },
                            {
                              "key":"MensKurta",
                              "value":"Kurta"
                            },
                            {
                              "key":"MensJacket",
                              "value":"Jacket"
                            }
                            
                          ]
                          
                        },
                        {
                          "catagory":"Bedding",
                          "subcatagory":[
                                          {"key":"Pillow",
                                            "value":"Pillow"
                                          },
                                          {"key":"BedSheets",
                                            "value":"BedSheets"
                                        }
                                      ]
                          
                        },
                        {
                          "catagory":"Watches",
                          "subcatagory":[
                            {
                              "key":"FastTrack",
                              "value":"FastTrack"
                            },
                            {
                              "key":"Casio",
                              "value":"Casio"
                            },
                            {
                              "key":"Fossil",
                              "value":"Fossil"
                            }
                           
                          ]
                          
                        },
                        {
                          "catagory":"WomensCloth",
                          "subcatagory":[
                            {
                              "key":"Sharee",
                              "value":"Sharee"
                            },
                            {
                              "key":"womensKurta",
                              "value":"Kurta"
                            },
                            {
                              "key":"womensJeans",
                              "value":"Jeans"
                            }
                          
                          ]
                          
                        }
                    
                      ]
      };
    }
   
   ngOnInit() : void {
    
      //this._product.getproducts() .subscribe(iproducts => this.iproducts = iproducts);
      this.itemList=this.store.getStoreItem();
 
      
   }
   //device type knowing
   epicFunction() {
    console.log('hello `Home` component');
    //this.deviceInfo = this.deviceService.getDeviceInfo();
    console.log(this.deviceInfo);
  }
   clicked(data)
   {
     if(data==1)
     {
      console.log("one clicked");
      this.first=this.buttonService.getSecond();
     }
     else if(data==2)
     {
      console.log("2 clicked");
      this.second=this.buttonService.getThird();
     }
     else{
      console.log("3 clicked");
      this.third=this.buttonService.getFirst();
     }
   }
   signup(data)
   {
    this.loginSpinner=true;
      setTimeout (() => {
        console.log("Hello from setTimeout 2o sec");
        this.loginSpinner=false;
      }, 20000);
      console.log("Hello from setTimeout");
      
   }

  //  By chaging link iner paramter was not changed so taking data from URL and checking url content
   searchList(catagory,subcatagory)
   {
     let documentInfo=catagory;
     console.log(this.router.url);
      var currentUrl=this.router.url;
      if(currentUrl.indexOf('selectedDocument')>=0)
      {
        this.router.navigateByUrl('/toggleSelectedDocument/'+documentInfo+"/"+subcatagory);
        
      }
      else
      {
        this.router.navigateByUrl('/selectedDocument/'+documentInfo+"/"+subcatagory);
      }
     
      this.pannelTogle();
      
   }
   pannelTogle()
   {
     if($("#nav-content").hasClass("show")) 
      {
        $("#nav-content").removeClass("show");
      }
   }
}

