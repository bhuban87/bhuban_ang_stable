//import { Injectable } from '@angular/core';

export class ItemsBean
{
    itemId:number;
    itemName:String;
    itemPrice:number;
    imageLocation:any=[];
    itemQuantity:number;
    itemCatagory:String;
    itemSubcatagory:String;
    discountValue:number;
    constructor(itemId: number, itemName: string,itemPrice:number,imageLocation:any,
        itemQuantity:number,itemCatagory:String,
        itemSubcatagory:String,discountValue:number) {
        this.itemId = itemId;
        this.itemName = itemName;
        this.itemPrice = itemPrice;
        this.imageLocation=imageLocation;
        this.itemQuantity=itemQuantity;
        this.itemCatagory=itemCatagory;
        this.itemSubcatagory=itemSubcatagory;
        this.discountValue=discountValue;
      }
}