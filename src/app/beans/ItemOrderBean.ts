export class ItemOrderBean
{
    orderId:number;
    userId:number;
    quantity:number;
    document:String;
    subDocument:String;
    custInfo:Object;
    constructor(orderId: number, userId: number,quantity:number,document:String,subDocument:String,custInfo:Object) {
        this.orderId = orderId;
        this.userId = userId;
        this.quantity = quantity;
       this.document=document;
       this.subDocument=subDocument;
       this.custInfo=custInfo;
      }
}