import { NgtUniversalModule } from '@ng-toolkit/universal';
import { CommonModule } from '@angular/common';
import {BrowserModule } from '@angular/platform-browser';
import { NgModule,NO_ERRORS_SCHEMA } from '@angular/core';
import {FormsModule} from '@angular/forms';

import { AppComponent } from './app.component';
import { HttpModule } from '@angular/http';

import { Product1 } from './product1';

import { Product2 } from './product2';

import { Product3 } from './product3';

import { myInputDir } from './DirectivesTest/input.field.dir';

import {Main} from './zoom/main';
import {MoveBackgroundDirective} from './zoom/MoveBackgroundDirective';
import {ImageZoom} from './zoom/image-zoom';

import { RouterModule, Routes } from '@angular/router';
import {SelectedProductItem} from './selectedProduct';


import {CartType} from './cart/CartType';
import {CartDataService} from './cart/cartDataService';
import {Cart} from './cart/cart';
import {CartShown} from './cart/cartShown';

import {FooComponent} from './Test/foo.component';
import {BarComponent} from './Test/bar.component';

import {NoSanitizePipe} from './filter/NoSanitizePipe';
import {ImageUpload} from './upload/ImageUpload';
import { HttpClient } from '@angular/common/http';
import { HttpClientModule } from '@angular/common/http';
import {NgxPaginationModule} from 'ngx-pagination';
import {PaggingDataItems} from './services/pagging.dataItems';
import { NgxImageGalleryModule } from 'ngx-image-gallery';
import { NgxGalleryModule } from 'ngx-gallery';
import {UserModule} from './userModule/user.module';
import{CheckOutItems} from './checkout/checkout';
import { LandingPage } from "./landing/LandingPage";
import {SlideshowModule} from 'ng-simple-slideshow';
import {UserInfo} from './userModule/user.info';
import { CommonComponent } from './common/common.component';
import { PaypalComponent } from './paypal/paypal.component';

const appRoutes: Routes = [
  { path: '', redirectTo: '/packyourbag', pathMatch: 'full'},
  { path: 'packyourbag', component: LandingPage },
  { path: 'Product1', component: Product1 },
  { path: 'Product2', component: Product2 },
  { path: 'selectedDocument/:searchDoc/:searchSubDoc', component: Product2 },
  { path: 'toggleSelectedDocument/:searchDoc/:searchSubDoc', component: Product2 },
  { path: 'Product3', component: Product3 },
  {path:'SelectedProduct/:itemId',component:SelectedProductItem},
  {path:'landingPageSelect/:itemId/:catagory/:subcatagory',component:SelectedProductItem},
  {path:'cart',component:Cart},
  {path:'upload',component:ImageUpload},
  {path:'checkout',component:CheckOutItems},
  {path:'paySuccess',component:PaypalComponent}
  
 

];
@NgModule({
  declarations: [
    AppComponent,
    myInputDir,
    Product1,
    Product2,
    Product3,
    LandingPage,
    SelectedProductItem,
    Main,
    MoveBackgroundDirective,
    ImageZoom,Cart,
    FooComponent,BarComponent,CartShown,NoSanitizePipe,ImageUpload,CheckOutItems, CommonComponent, PaypalComponent
   
  ],
  imports:[
 CommonModule,
NgtUniversalModule,HttpModule, HttpClientModule,
    RouterModule.forRoot(appRoutes,{ useHash: true }),NgxPaginationModule,NgxImageGalleryModule,NgxGalleryModule,
    UserModule,SlideshowModule
    
    
  ],
  
  providers: [PaggingDataItems],
  exports:[myInputDir,Product1,Product2,Product3,SelectedProductItem,ImageUpload,RouterModule]
})
export class AppModule { 
  
}