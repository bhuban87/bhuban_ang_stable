import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import {UserResponse} from './UserResponse';
import { Constants } from '../common/common.component';
import { Observable, Subject, asapScheduler, pipe, of, from, interval, merge, fromEvent } from 'rxjs';
@Injectable()
export class UserService {
   
    constructor(private http: HttpClient) {
    }
    saveUser(obj) : Observable<UserResponse>
    {
        return this.http.post<UserResponse>(Constants.API_ENDPOINT+"addUser", obj);
    }
}