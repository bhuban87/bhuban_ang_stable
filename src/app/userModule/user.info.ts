import { Component, EventEmitter, Input, Output ,Inject } from '@angular/core';

import { RouterModule, Routes,Router,NavigationExtras ,Params } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import {UserService} from './user.service';
import * as $ from 'jquery';

@Component ({
   selector: 'user-info',
   templateUrl: './user.info.html',
   providers:[]
   
})
export   class   UserInfo {
   
    userName:String=null;
    passWord:String=null;
    phoneNumber:Number=null;
    email:String=null;
    obj: any; 
    suucessLogMesg=null;
    showSpinner=false;
    public signInTab: boolean=true;
    constructor(private userService:UserService) {
      this.signInTab=true;
     }
     saveUser()
     {
       this.showSpinner=true;
       if((this.email || this.userName) && this.passWord)
       {
            this.obj={
              "userName":this.userName,
              "passWord":this.passWord,
              "mobNumber":this.phoneNumber,
              "emailId":this.email
          };
          this.userService.saveUser(this.obj).subscribe(res => { 
            
              console.log("hello save user");	
              this.suucessLogMesg="Logged in Successfuly.";	
              this.showSpinner=false;
            },
            err => {
              console.log(err);
              this.showSpinner=false;
            }
        );
       }
       else
       {
        this.showSpinner=false;
        this.suucessLogMesg="Provide username and password.";	
       }
        
     }
     cancel()
     {
      this.userName=null;
      this.passWord=null;
      this.phoneNumber=null;
      this.email=null;
     }
     clicked(event,id) 
     {
       
      if(id=='exUser')
      {
        if($("#newUser").hasClass('active'))
        {
          $("#newUser").removeClass('active');
        }
        if(!$("#exUser").hasClass('active'))
        {
          $("#exUser").addClass('active');
        }
        this.signInTab=true;
      }
      else if(id=='newUser')
      {
        if($("#exUser").hasClass('active'))
        {
          $("#exUser").removeClass('active');
        }
        if(!$("#newUser").hasClass('active'))
        {
          $("#newUser").addClass('active');
        }
        this.signInTab=false;
      }
     
      /*
      var m=event.target.classList.contains('active'); // To check
      if(m)
      {
        var y=event.target.classList.remove('active'); // To Remove
      }
      else
      {
        var x=event.target.classList.add('active'); // To ADD
      }*/
      //var z=event.target.classList.toggle('active'); // To toggle
    }
}