import { NgModule,NO_ERRORS_SCHEMA } from '@angular/core';
import {UserInfo} from './user.info';
import { CommonModule } from '@angular/common';  
import { BrowserModule } from '@angular/platform-browser';
import {UserService} from './user.service';
import { RouterModule, Routes } from '@angular/router';
const secondaryRoutes: Routes = [
  { path: 'signin',  component:UserInfo }
  
];
@NgModule({
    declarations: [
        UserInfo
         ],
    imports: [
      CommonModule,
      RouterModule.forChild(secondaryRoutes)
    ],
    providers: [UserService],
    exports:[UserInfo,RouterModule]
   
    
  })

export class UserModule { 
  
}