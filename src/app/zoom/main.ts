import {Component, Input} from '@angular/core';

@Component({
  selector: 'main',
  templateUrl: 'main.html'
})

export class Main {
  
  srcImage: string;
  srcImageZoom: string;
  showZoom: boolean;
  xPos: number;
  yPos: number;

  constructor() {
    this.showZoom = false;
    this.srcImage = "assets/images/amazon_1.jpg";
    this.srcImageZoom = "assets/images/amazon_2.jpg";
  }

  openZoom(event: any, show: boolean) {
    event.preventDefault();
    this.showZoom = show;
    if (show) {
      this.positionZoom(event);
    }
  }

  positionZoom(event) {
    let xPos = event.touches[0].pageX - event.touches[0].target.offsetLeft
    let yPos = event.touches[0].pageY - event.touches[0].target.offsetTop;
    let xMax = event.target.clientWidth;
    let yMax = event.target.clientHeight;
    this.xPos = this.validPercent(Math.round(xPos * 100 / xMax));
    this.yPos = this.validPercent(Math.round(yPos * 100 / yMax));
  }

  validPercent(value) {
    if (value < 0) {
      this.showZoom = false;
      return 0;
    };
    if (value > 100) {
      this.showZoom = false;
      return 100
    };
    return value;
  }

}